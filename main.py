import fetch_data
import experience_barchart
import normalized_games


def fetch_and_draw_1vs1():
    filename = "random_map_data.csv"
    leaderboard_id = 3
    ranks, ratings, games, wins, losses = fetch_data.main(leaderboard_id)
    fetch_data.save(filename, ranks, ratings, games, wins, losses)
    duel_params = (
        "random_map_data.csv",
        [(0, 700), (700, 800), (800, 900), (900, 1000), (1000, 1100),
        (1100, 1200), (1200, 1300), (1300, 1400), (1500, 9999)],
        [(0, 20), (20, 50), (50, 100), (100, 200), (200, 9999)],
        ["Under 20 games", "20 - 50", "50 - 100", "100 - 200", "Over 200 games"],
        "Games played and Elo ratings in AoE II: DE 1vs1 Random Map Leaderboard"
    )
    experience_barchart.draw_bar_chart(*duel_params)


def fetch_and_draw_team():
    filename = "team_random_map.csv"
    leaderboard_id = 4
    ranks, ratings, games, wins, losses = fetch_data.main(leaderboard_id)
    fetch_data.save(filename, ranks, ratings, games, wins, losses)
    tg_params = (
        "team_random_map.csv",
        [(0, 800), (800, 900), (900, 1000), (1000, 1100), (1100, 1200),
         (1200, 1300), (1300, 1400), (1400, 1500), (1500, 1600), (1600, 1700),
         (1700, 1800), (1800, 1900), (1900, 2000), (2000, 9999)],
        [(0, 20), (20, 50), (50, 100), (100, 200), (200, 300), (300, 9999)],
        ["Under 20 games", "20 - 50", "50 - 100", "100 - 200", "200 - 300", "Over 300 games"],
        "Games played and Elo ratings in AoE II: DE Team Random Map Leaderboard"
    )
    experience_barchart.draw_bar_chart(*tg_params)


def fetch_and_draw_normalized():
    filename = "random_map_data.csv"
    leaderboard_id = 3
    ranks, ratings, games, wins, losses = fetch_data.main(leaderboard_id)
    fetch_data.save(filename, ranks, ratings, games, wins, losses)
    normalized_games.draw_normalized_barchart(
        "random_map_data.csv",
        [(0, 800), (800, 1000), (1000, 1200), (1200, 1400), (1400, 1600), (1600, 9999), (0, 9999)],
        [(10, 50), (50, 100), (100, 200), (200, 500), (500, 9999)],
        ["10-49", "50-99", "100-199", "200-499", "500-"],
        "How many games players have played on different Elo rating?"
    )

if __name__ == '__main__':
    fetch_and_draw_normalized()
