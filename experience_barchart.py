import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime


def draw_bar_chart(datafile, ratings, games, labels, title):
    # plt.style.use('fivethirtyeight') # Let's not use this style for time being.
    data = pd.read_csv(datafile)
    x_coordinates = np.arange(len(ratings))
    bardata = [[] for _ in games]
    players_with_experience = []
    for i, (low_g, high_g) in enumerate(games):
        # First we separate the data based on number of games played ie. experience
        games_f = data[(data["games"] >= low_g) & (data["games"] < high_g)]
        players_with_experience.append(games_f.shape[0])
        for j, (low_r, high_r) in enumerate(ratings):
            # Then, each bucket of experiences is separated based on Elo rating
            rating_data = games_f[(games_f["rating"] >= low_r) & (games_f["rating"] < high_r)]
            bardata[i].append(rating_data.shape[0])
    # Y-padding determines how high the next section of the bar chart should be.
    # In other words, total number of players below the next bucket/category of experiences
    y_padding = [0] * len(ratings)
    bar_objects = []
    for i in range(len(bardata)):
        bar = plt.bar(x_coordinates, bardata[i], .75, bottom=y_padding, label=labels[i], zorder=3)
        bar_objects.append(bar)
        for y in range(len(y_padding)):
            # Don't forget to increase the vertical offset for each bar
            y_padding[y] += bardata[i][y]
    # Annotate the figure
    # How big portion each experience group is from the total players in that elo rating?
    tmp = 0
    for bar_obj in zip(*bar_objects):
        heights = [b.get_height() for b in bar_obj]
        text_x = [b.get_x() + b.get_width() / 2 for b in bar_obj]
        text_y = []
        for i in range(len(heights)):
            text_y.append(heights[i]/2 + sum(heights[:i]))
            if len(text_y) >= 2 and text_y[i] - text_y[i-1] < 120:
                text_y[i] += 120 - (text_y[i] - text_y[i-1])
            #bartext = "{0:.1f}%".format((heights[i] / y_padding[tmp]) * 100)
            bartext = heights[i]
            plt.text(text_x[i], text_y[i], bartext,
                     ha="center", va="center", color="black",
                     fontsize=9, fontweight="bold")
        tmp += 1
    # Tick marks and titles
    plt.title(title)
    plt.ylabel("Number of players")
    plt.xlabel("\nElo Ratings\nPercentage of players inside each Elo range from all rated players")
    # Note: First and last tick of the X-axis are different to others
    tick_labels = ["- {0}\n{1:.1f}% of all\nranked players".format(ratings[0][1], 100*y_padding[0]/data.shape[0])]
    for i in range(1, len(ratings)-1):
        tick_labels.append("{0} - {1}\n{2:.1f}%"
                           .format(ratings[i][0], ratings[i][1]-1, 100*y_padding[i]/data.shape[0]))
    tick_labels.append("{0} -\n{1:.1f}%".format(ratings[-1][0], 100*y_padding[-1]/data.shape[0]))
    plt.xticks(x_coordinates, tick_labels)
    plt.legend(title="Games played")
    # Add timestamp & signature
    signature = "Data gathered from aoe2.net\nCreated in {0:%d.%m.%Y} by /u/DaaxD".format(datetime.datetime.now())
    # The position is handcrafted. It's easier tweak it by hand compared to figuring out a specific formula for it.
    # Especially since it's used only once.
    plt.text(x_coordinates[-2]+.1, -800, signature, fontsize=10, multialignment="right")
    plt.minorticks_on()
    plt.grid(True, axis='y',which='both', zorder=1)
    plt.grid(True, axis='y', which='major', zorder=1)
    plt.show()


if __name__ == '__main__':
    tg_params = (
        "team_random_map.csv",
        [(0, 800), (800, 900), (900, 1000), (1000, 1100), (1100, 1200),
         (1200, 1300), (1300, 1400), (1400, 1500), (1500, 1600), (1600, 1700),
         (1700, 1800), (1800, 1900), (1900, 2000), (2000, 2100), (2100, 2200), (2200, 9999)],
        [(0, 20), (20, 50), (50, 100), (100, 200), (200, 300), (300, 400), (400, 9999)],
        ["Under 20 games", "20 - 50", "50 - 100", "100 - 200", "200 - 300", "300 - 400","Over 400 games"],
        "Games played and Elo ratings in AoE II: DE Team Random Map Leaderboard"
    )
    duel_params = (
        "random_map_data.csv",
        [(0, 700),(700, 800), (800, 900), (900, 1000), (1000, 1100),
        (1100, 1200), (1200, 1300), (1300, 1400), (1400, 1500),(1500, 9999)],
        [(0, 25), (25, 50), (50, 100), (100, 200), (200, 400),(400, 9999)],
        ["Less than 25", "25-49", "50-99", "100-199", "200-399", "400 or more"],
        "Games played and Elo ratings in AoE II: DE 1vs1 random Map leaderboard"
    )
    draw_bar_chart(*duel_params)
