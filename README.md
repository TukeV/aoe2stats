# AoE II:DE Elo Distribution and Player Eperience #

Here is a collection of scripts I use to visualize the elo distribution and experience of AoE II:DE players. The repository includes scripts for pulling the data from aoe2.net API, drawing the raw Elo distribution and drawing a bar chart of how experiences players are on different skill levels.

I do occasionally use post the graphs produced by these scripts on AoE II subreddit and official AoE II:DE, so I really rather you didn't steal my karma withotu credting me.

- Reddit: /u/DaaxD

- Official AoE:II Forums: ClosingIslands58

## Usage ###

The scripts here do not have proper UI (neither commandline or graphical), so user would have to do some handwork to get these scripts running. I run this scripts insice pycharm, where they can be easily edited by hand.

Someone else might want to automate some of these handcrafted parameters, but I'm not that kind of person.

### Downloading the data

The most recent data can be downloaded by using *fetch_data.py*. At bottom of the script (line 43) you can decide which leaderboard you want to download and to what file you want to download it's data. 

### Draw the raw data

The individual players and their experiecne can be drawn with *raw_data_plot.py*, but this script does not IMHO give too much insight about the player base.

### Elo Distribution

The *plyaerbase_distribution.py* draws the whole playerbase as a barchart. By design, the player distribution in a Elo systems should gravitate to something resembling a bell curve.

### Playerbase and experience

There are two scripts which explores the relation between Elo rating and the experience. First one is *experience_barchart.py*, which draws the whole player base just like the *player_distribution.py*, but it also further divides the playerbase in a buckets or bins based on their experience rating. 

The *normalized_barchart.py* draws a barchart, which shows what is the protion of each experience level inside the Elo range.

The Elo and experience ranges of each script can be configured by editing the parameters at the bottom of the script. These are made by hand for design reasons (and also due the laziness).

