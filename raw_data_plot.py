from matplotlib import pyplot as plt
import pandas as pd
import numpy as np


def main(filename):
    df = pd.read_csv(filename)
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    ax1.plot(df["rating"], df["games"], '.')
    ax1.set_title("How many games people with different ratings have played")
    ax1.set_xlabel("Player Rating")
    ax1.set_ylabel("Games Played")

    winrate = np.divide(df["wins"], df["games"])
    ax2.plot(df["rating"], winrate, '.', label='Win Rate')
    ax2.set_title("Win rates on different ratings")
    ax2.set_ylabel("Win rate")
    ax2.set_xlabel("Player Rating")
    ax2.legend()
    plt.show()


if __name__ == '__main__':
    # duels = '1vs1random_map.csv'
    tg = "team_random_map.csv"
    main(tg)
