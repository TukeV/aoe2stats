import requests
import json
import time


def main(leaderboard_id):
    real_player_count = None
    player_count = 20000
    ranks = []
    ratings = []
    games = []
    wins = []
    losses = []
    i = 1
    while i < player_count:
        print("Fetching {} / {}".format(i,  player_count))
        url = "https://aoe2.net/api/leaderboard?leaderboard_id={}&start={}&count=1000".format(leaderboard_id, i)
        r = requests.get(url)
        data = json.loads(r.content)
        if real_player_count is None:
            real_player_count = data["total"]
            player_count = real_player_count
        for entry in data['leaderboard']:
            ratings.append(entry['rating'])
            games.append(entry['games'])
            ranks.append(entry['rank'])
            wins.append(entry['wins'])
            losses.append(entry['losses'])
        i += 1000
    return ranks, ratings, games, wins, losses


def save(filename, ranks, ratings, games, wins, losses):
    with open(filename, 'w') as fin:
        fin.write("rank,rating,games,wins,losses\n")
        for rank, rating, game, win, loss in zip(ranks, ratings, games, wins, losses):
            fin.write("{}, {}, {}, {}, {}\n".format(rank, rating, game, win, loss))


if __name__ == '__main__':
    RANDOM_MAP = ("random_map_data.csv", 3)
    TEAM_GAME = ("team_random_map.csv", 4)
    filename, leaderboard = TEAM_GAME
    ranks, ratings, games, wins, losses = main(leaderboard)
    save(filename, ranks, ratings, games, wins, losses)