import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def draw_bar_chart(datafile, ratings, categories, labels, title):
    data = pd.read_csv(datafile)
    indices = np.arange(len(ratings))
    bardata = [[] for _ in categories]
    #plt.style.use('fivethirtyeight')
    winrates = data["wins"] / data["games"]
    data.insert(5, "winrate", winrates)
    for i, (low_w, high_w) in enumerate(categories):
        winrate_f = data[(data["winrate"] > low_w) & (data["winrate"] <= high_w)]
        for j, (low_r, high_r) in enumerate(ratings):
            rating_data = winrate_f[(winrate_f["rating"] > low_r) & (winrate_f["rating"] <= high_r)]
            try:
                bardata[i].append(rating_data.shape[0])
            except IndexError:
                bardata[i].append(0)
    y_padding = [0] * len(ratings)
    for i in range(len(bardata)):
        plt.bar(indices, bardata[i], .75, bottom=y_padding, label=labels[i])
        for y in range(len(y_padding)):
            y_padding[y] += bardata[i][y]
    plt.title(title)
    plt.ylabel("Number of players")
    plt.xlabel("Elo Rating")
    tick_labels = ["- {}".format(ratings[0][1])]
    for i in range(1, len(ratings)-1):
        tick_labels.append("{} - {}".format(*ratings[i]))
    tick_labels.append("{} -".format(ratings[-1][0]))
    plt.xticks(indices, tick_labels)
    plt.legend(title="Win %")
    plt.show()


if __name__ == '__main__':
    duel_parameters = (
        "1vs1random_map.csv",
        [(0, 700), (700, 800), (800, 900), (900, 1000), (1000, 1100),
         (1100, 1200), (1200, 1300), (1300, 1400), (1400, 1500), (1500, 9999)],
        [(-1, .3), (.3, .4), (.4, .5), (.5, .6), (.6, .7), (.7, 1.1)],
        ["< 30%", "30% - 40%", "40% - 50%", "50% - 60%", "60% - 70%", "> 70%"],
        "Win Rates In AoE II: DE 1vs1 Random Map Leaderboard"
    )
    tg_parameters = (
        "team_random_map.csv",
        [(0,800), (800, 900), (900, 1000), (1000, 1100), (1100, 1200), (1200, 1300), (1300, 1400),
         (1400, 1500), (1500, 1600), (1600, 1700), (1700, 1800), (1800, 1900), (1900, 2000), (2000, 9999)],
        [(-1, .3), (.3, .4), (.4, .5), (.5, .6), (.6, .7), (.7, 1.1)],
        ["< 30%", "30% - 40%", "40% - 50%", "50% - 60%", "60% - 70%", "> 70%"],
        "Win Rates In AoE II: DE Team Random Map Leaderboard"
    )
    draw_bar_chart(*duel_parameters)
