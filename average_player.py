import numpy as np
import pandas as pd


def rating_of_average_player(data):
    game_total = np.sum(data['games'])
    games_x_rating = np.multiply(data['games'], data['rating'])
    average_player = np.sum(games_x_rating) / game_total
    print("The average player has elo rating of {0:.0f}".format(float(average_player)))


def average_played_games(data):
    average_games = np.sum(data["games"]) / data["games"].shape[0]
    print("Average number of games played: {0:.0f}".format(average_games))


def average_elo(data):
    elo_sum = np.sum(data["rating"])
    player_count = data["rating"].shape[0]
    print("The average rating is {0:.0f}".format(elo_sum / player_count))


if __name__ == '__main__':
    data = pd.read_csv('team_random_map.csv')
    rating_of_average_player(data)
    average_played_games(data)
    average_elo(data)
