import matplotlib.pyplot as plt
import pandas as pd
import datetime


def elo_distribution(datafile, bucket_ranges, labels):
    """
    Draws the contents of the datafile as a bar chart, where the data is broken down into different ranges.
    :param datafile: A path to the data source. This expected to be a csv file which has a column "rating", which
    includes the ratings of the players.

    The file I used had following structure:

    rank,rating,games,wins,losses
    1, 2256, 1237, 848, 389
    2, 2240, 411, 290, 121
    3, 2233, 515, 387, 128
    ...

    But I guess any file with column called "rating" would do just fine, as long as pandas can parse it correctly.

    :param bucket_ranges: List of tuples, which specifies the interval for each player bucket. The first value
        of the tuple is the lower limit and the second value is the upper limit of the bucket's interval

    :param labels: Custom ticks labels drawn under each bar chart. It's recommended that there are as many labels
        as there are intervals in the bucket_ranges list.
    """
    data = pd.read_csv(datafile)
    buckets = []
    # Find how many players is inside of each range ie. belongs in each bucket
    buckets = []
    for low, high in bucket_ranges:
        rows = data[(data["rating"] >= low) & (data["rating"] <= high)]
        buckets.append(len(rows.index))
    # Start plotting
    indices = list(range(len(bucket_ranges))) # Using numpy's arrange would look cleaner
    bars = plt.bar(indices, buckets, .85, zorder=3)
    # Annotate bars with cumulative percentiles
    size = len(data.index)
    fontsize = 10
    for i, bar in enumerate(bars):
        # Put the text in the middle of the bar, unless the bar is too short.
        # 75 and 300 are magic numbers used to make things look neat.
        # Smarter person would probably try to calculate these numbers based on chart's proportions.
        text_x = bar.get_x() + bar.get_width() / 2
        text_y = buckets[i] + 150 if buckets[i] < 300 else bar.get_height() / 2
        # Calculate the cumulative percentage...
        bar_percentage = (buckets[i]/size)*100
        cumper = (sum(buckets[:i+1])/size)*100
        text = "{0:.1f}%\n({1:.1f}%)".format(bar_percentage, cumper)
        # ... and draw it
        plt.text(text_x, text_y, text, fontsize=fontsize,
                 ha="center", va="center", color="black", fontweight="bold")
    # Decorate the rest
    plt.title("Elo Distribution In AoE II:DE 1vs1 Leaderboard")
    plt.legend()
    plt.ylabel("Number of Players")
    plt.xlabel("Elo Rating")
    plt.xticks(indices, labels, rotation=45)
    # For some reason matplotlib behaves like a vogon, when you want to draw minor grid.
    plt.minorticks_on()
    plt.grid(True, axis='y',which='both', zorder=1)
    plt.grid(True, axis='y', which='major', zorder=1)
    # Add date and signature
    signature = "Data gathered from aoe2.net\nCreated in {0:%d.%m.%Y} by /u/DaaxD".format(datetime.datetime.now())
    # The location is handcrafted, hence the magic numbers.
    plt.text(indices[-1], -800, signature, fontsize=9, multialignment="right")
    # Tadaa!
    plt.show()


def get_labels(buckets):
    labels = ["- {}".format(buckets[0][1])]
    for low, high in buckets[1:-1]: labels.append("{}-{}".format(low, high))
    labels.append("{} -".format(buckets[-1][0]))
    return labels


if __name__ == '__main__':
    duel_parameters = ("random_map_data.csv",
                [(-1, 399),(400, 499), (500, 599), (600, 699), (700, 799), (800, 899),
                     (900, 999), (1000, 1099), (1100, 1199), (1200, 1299), (1300, 1399),
                     (1400, 1499), (1500, 1599), (1600, 1699), (1700, 1799), (1800, 1899),
                     (1900, 1999), (2000, 99999)])

    tg_parameters = ("team_random_map.csv",
        [ (0, 799), (800, 899), (900, 999), (1000, 1099), (1100, 1199),
         (1200, 1299), (1300, 1399), (1400, 1499), (1500, 1599), (1600, 1699),
         (1700, 1799), (1800, 1899), (1900, 1999), (2000, 2099), (2100, 2199),
         (2200, 2299), (2300, 9999)])
    # Prepare the labels. The First and the last labels are different than the rest of them.
    labels = get_labels(duel_parameters[1])
    # Let's start cooking
    elo_distribution(*duel_parameters, labels)
