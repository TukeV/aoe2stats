import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
import datetime


def players_in_elo_range(data, elo_ranges):
    elo_buckets = []
    for lower, upper in elo_ranges:
        elo_buckets.append(data[(data["rating"] >= lower) & (data["rating"] < upper)])
    return elo_buckets


def experiences_in_bucket(bucket, game_ranges):
    experience_bucket = []
    for lower, upper in game_ranges:
        players_w_experience = bucket[(bucket["games"] >= lower) & (bucket["games"] < upper)]
        number_of_players = len(players_w_experience)
        percentage = (number_of_players / len(bucket))*100
        experience_bucket.append(percentage)
    return experience_bucket


def draw_normalized_barchart(datafile, elo_ranges, game_ranges, labels, title):
    #plt.style.use('fivethirtyeight')
    data = pd.read_csv(datafile)
    # bardata is 2 dimensional list, where each item contains list of percentages
    bardata = [[] for _ in game_ranges]
    # Divide players into elo categories
    elo_buckets = players_in_elo_range(data, elo_ranges)
    portion_of_players = [100*len(x)/len(data) for x in elo_buckets]
    # Find out the experience proportions in each categories for each experience range
    for bucket in elo_buckets:
        experiences_in_elo_range = experiences_in_bucket(bucket, game_ranges)
        for i, exp in enumerate(experiences_in_elo_range):
            bardata[i].append(exp)
    # plot bardata
    # x values for barchart
    indices = np.arange(0, len(elo_ranges)*2, 2)
    # The bar for whole player base is a bit further than rest of the bars
    bars = []
    indices[-1] += 1
    y_padding = [0] * len(elo_ranges)
    for i in range(len(bardata)):
        bar = plt.bar(indices, bardata[i], 1.5, bottom=y_padding, label=labels[i], zorder=3)
        bars.append(bar)
        for y in range(len(y_padding)):
            y_padding[y] += bardata[i][y]
    # Annotate
    fontsize = 12
    tmp = 0
    for bar_obj in zip(*bars):
        heights = [b.get_height() for b in bar_obj]
        text_x = [b.get_x() + b.get_width() / 2 for b in bar_obj]
        text_y = []
        for i in range(len(heights)):
            text_y.append(heights[i] / 2 + sum(heights[:i]))
            if len(text_y) >= 2.5 and text_y[i] - text_y[i-1] < 2:
                text_y[i] += 2.5 - (text_y[i] - text_y[i-1])
            bartext = "{0:.1f}%".format(heights[i])
            plt.text(text_x[i], text_y[i], bartext,
                     ha="center", va="center", color="black",
                     fontsize=fontsize, fontweight="bold")
        tmp += 1

    # Other cosmetics
    plt.title(title)
    plt.legend(title="Games played")
    plt.ylabel("Percentage of players")
    plt.xlabel("\nElo Rating")
    tick_labels = ["- {0}\n{1:.1f}% of players".format(elo_ranges[0][1], portion_of_players[0])]
    for i in range(1, len(elo_ranges) - 2):
        tick_labels.append("{0} - {1}\n{2:.1f}% of players".format(*elo_ranges[i], portion_of_players[i]))
    tick_labels.append("{0} - \n{1:.1f}% of players".format(elo_ranges[-2][0], portion_of_players[-2]))
    tick_labels.append("Whole player base.")
    plt.xticks(indices, tick_labels)
    yticks = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    plt.yticks(yticks, ["{}%".format(x) for x in yticks])
    plt.ylim(0, 104)
    plt.xlim(-1.5, indices[-1]+3)
    # singature
    signature = "Data gathered from aoe2.net\nCreated in {0:%d.%m.%Y} by /u/DaaxD".format(datetime.datetime.now())
    plt.text(indices[-1], -10, signature, fontsize=9, multialignment="right")
    plt.minorticks_on()
    plt.grid(True, axis='y',which='both', zorder=1)
    plt.grid(True, axis='y', which='major', zorder=1)
    plt.show()


if __name__ == '__main__':
    #draw_normalized_barchart(
    #    "random_map_data.csv",
    #    [(0, 800), (800, 1000), (1000, 1200), (1200, 1400), (1400, 1600), (1600, 9999), (0, 9999)],
    #    [(10, 50), (50, 100), (100, 200), (200, 500), (500, 9999)],
    #    ["10-49", "50-99", "100-199", "200-499", "500-"],
    #    "Elo score and games played in AoE II:DE 1vs1 leaderboard"
    #)
     draw_normalized_barchart(
        "random_map_data.csv",
        [(0, 700), (700, 900), (900, 1100), (1100, 1300), (1300, 1500), (1500, 9999), (0, 9999)],
        [(0, 25), (25, 50), (50, 100), (100, 200), (200, 400),(400, 9999)],
        ["Less than 25", "25-49", "50-99", "100-199", "200-399", "400 or more"],
        "Elo score and games played in AoE II:DE 1vs1 leaderboard"
     )